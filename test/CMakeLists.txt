# include(Catch) // i have no idea why this does not work
include_directories(${CATCH2_INC_DIR})
set(TEST_INC ${CMAKE_CURRENT_SOURCE_DIR}/inc)
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/build/_deps/catch2-src/extras")

enable_testing()

file(GLOB_RECURSE TEST_SOURCES_FILES
    ${CMAKE_CURRENT_SOURCE_DIR}/inc/*.c
    ${CMAKE_CURRENT_SOURCE_DIR}/src/*.cpp)

if(TEST_SOURCES_FILES)
    include_directories(${LIBS_INC_DIR} ${CATCH2_INC_DIR} ${TEST_INC})
    add_executable(${TEST_EXECUTABLE_NAME} ${TEST_SOURCES_FILES})
    include_directories(${TEST_INCLUDES})
    target_link_libraries(${TEST_EXECUTABLE_NAME} PUBLIC Catch2::Catch2WithMain)

    if(EXISTS ${LIBS_OUT_FILE})
        message(STATUS "Linking state: ${TEST_EXECUTABLE_NAME} vs ${LIBS_NAME}")
        target_link_libraries(${TEST_EXECUTABLE_NAME} PUBLIC ${LIBS_NAME})
    endif()
    include(Catch)
    catch_discover_tests(${TEST_EXECUTABLE_NAME})
endif()
