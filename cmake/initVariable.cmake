set(CMAKE_EXPORT_COMPILE_COMMANDS ON) 

if(NOT CMAKE_CXX_COMPILER)
    set(CMAKE_CXX_COMPILER "g++")
endif()

if(NOT CMAKE_C_COMPILER)
    set(CMAKE_C_COMPILER "gcc")
endif()

set(COMPILER_WARNINGS_OPTIONS  -O0 -g -Wall -Wpedantic -Wconversion -Wsign-conversion -Wunreachable-code -Weverything)

if(DEFINED CMAKE_C_STD)
    set(CMAKE_C_STANDARD ${CMAKE_C_STD})
else()
    set(CMAKE_C_STANDARD 99)
endif()

if(DEFINED CMAKE_CXX_STD)
    set(CMAKE_CXX_STANDARD ${CMAKE_CXX_STD})
else()
    set(CMAKE_CXX_STANDARD 11)
endif()

if(NOT CMAKE_BINARY_DIR)
    set(CMAKE_BINARY_DIR ${CMAKE_SOURCE_DIR}/build)
endif()

if(NOT APP_EXECUTABLE_NAME)
    set(APP_EXECUTABLE_NAME "appDefaultName")
endif()

if(NOT TEST_EXECUTABLE_NAME)
    set(TEST_EXECUTABLE_NAME "testDefaultName")
endif()

if(DEFINED LIBS_NAME_PR)
    set(LIBS_NAME "${LIBS_NAME_PR}")
    else()
    set(LIBS_NAME "libsDefaultName")
endif()

set(CMAKE_C_STANDARD_REQUIRED ON)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(EXTERN_MODULE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/extern_modules")

set(LIBS_DIR "${CMAKE_CURRENT_SOURCE_DIR}/libs")
set(APP_DIR "${CMAKE_CURRENT_SOURCE_DIR}/app")
set(TEST_DIR "${CMAKE_CURRENT_SOURCE_DIR}/test")
set(CATCH2_INC_DIR "${CMAKE_BINARY_DIR}/_deps/catch2-src/src")
set(LIBS_OUT_FILE ${CMAKE_BINARY_DIR}/libs/src/${LIBS_NAME}.lib)

set(LIBS_INC_DIR ${LIBS_DIR}/inc)
set(LIBS_SRC_DIR ${LIBS_DIR}/src)
set(${LIB_SUFFIX} "OUT")