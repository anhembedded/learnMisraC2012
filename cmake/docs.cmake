#find_package(Doxygen
 #   REQUIRED dot
  #  OPTIONAL_COMPONENTS mscgen dia)

if(DOXYGEN_FOUND)
    add_custom_target(
        docs
        ${DOXYGEN_EXECUTABLE}
        WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}/Doxyfile
    )
endif()