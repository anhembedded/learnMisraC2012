include(FetchContent)
FetchContent_Declare(
    Catch2
    GIT_REPOSITORY https://github.com/catchorg/Catch2
    GIT_TAG v3.5.3
    GIT_SHALLOW TRUE)
FetchContent_MakeAvailable(Catch2)
list(APPEND CMAKE_MODULE_PATH ${catch2_SOURCE_DIR}/extras
    ${CMAKE_CURRENT_SOURCE_DIR}/cmake)

set(CATCH2_INC_DIR "${CMAKE_BINARY_DIR}/_deps/catch2-src/src")
add_subdirectory(${TEST_DIR})