cmake_minimum_required(VERSION 3.26)
project(Coding_practice LANGUAGES C CXX)

include(cmake/initVariable.cmake)
include(cmake/docs.cmake)

add_subdirectory(${LIBS_DIR})
add_subdirectory(${APP_DIR})

if(UNIT_TEST)
    include(cmake/fetchExternalModule.cmake)    
endif()

message(STATUS "-----------------[ CMAKE CONFIG INFO ]--------------------")
message(STATUS "CMAKE_C_COMPILER    : " ${CMAKE_C_COMPILER_ID})
message(STATUS "CMAKE_CXX_COMPILER  : " ${CMAKE_CXX_COMPILER_ID})
message(STATUS "BUILD_TYPE          : " ${CMAKE_BUILD_TYPE})
message(STATUS "C_FLAGS             : " ${CMAKE_CXX_FLAGS})
message(STATUS "CXX_FLAGS           : " ${CMAKE_C_FLAGS})
message(STATUS "CXX_STANDARD        : " ${CMAKE_CXX_STANDARD})
message(STATUS "C_STANDARD          : " ${CMAKE_C_STANDARD})
message(STATUS "TEST_EXECUTABLE_NAME: " ${TEST_EXECUTABLE_NAME})
message(STATUS "APP_EXECUTABLE_NAME : " ${APP_EXECUTABLE_NAME})
message(STATUS "------------------[ END LOG ]------------------------------")



