# Setting your project
U_CMAKE_C_STD=11 # 99, 11 ..
U_CMAKE_CXX_STD=17 # 99, 11, 14, 17
U_CMAkE_GENERATOR="MinGW Makefiles" # "MinGW Makefiles" for mingw32-64, "Unix Makefiles" for linux GNU
APP_EXECUTABLE_NAME=myApp # name of your app
TEST_EXECUTABLE_NAME=unitTest # name of your test
LIBS_NAME_PR=myLib # name of your lib

# Configuration
MAKE="mingw32-make" # path to your Make tool
U_PROJECT_NAME=TEST_
U_CMAKE_BINARY_DIR=build # path to your build output
U_CMAKE_APP_OUTPUT_DIR="app" #not used yet
U_CMAKE_TEST_OUTPUT_DIR="unit_test" #not used yet
U_CMAKE_CXX_COMPILER="clang++" # path to your C++ compiler
U_CMAKE_C_COMPILER="clang" # path to your C compiler

U_CMAKE_DEFINE = 	-DTEST_EXECUTABLE_NAME=${TEST_EXECUTABLE_NAME} \
			 		-DAPP_EXECUTABLE_NAME=${APP_EXECUTABLE_NAME} \
					-DCMAKE_C_STD=${U_CMAKE_C_STD} \
					-DCMAKE_CXX_STD=${U_CMAKE_CXX_STD} \
					-DLIBS_NAME_PR=${LIBS_NAME_PR} \
					-DCMAKE_CXX_COMPILER=${U_CMAKE_CXX_COMPILER} \
					-DCMAKE_C_COMPILER=${U_CMAKE_C_COMPILER}

BUILD_DIR=${U_CMAKE_BINARY_DIR}
DEPENDENCY_GRAPH=dependencyGraph


config_debug:
	cmake -B ${U_CMAKE_BINARY_DIR} -S ./ -G ${U_CMAkE_GENERATOR} ${U_CMAKE_DEFINE} -DCMAKE_BUILD_TYPE=Debug -DUNIT_TEST=OFF
config_release:
	cmake -B ${U_CMAKE_BINARY_DIR} -S ./ -G ${U_CMAkE_GENERATOR} ${U_CMAKE_DEFINE}  -DCMAKE_BUILD_TYPE=Release -DUNIT_TEST=OFF
config_test:
	cmake -B ${U_CMAKE_BINARY_DIR} -S ./ -G ${U_CMAkE_GENERATOR} ${U_CMAKE_DEFINE} -DCMAKE_BUILD_TYPE=Debug -DUNIT_TEST=ON
build_test: config_test
	$(MAKE) -C ${U_CMAKE_BINARY_DIR}
run_test: build_test
	./build/test/${TEST_EXECUTABLE_NAME}
build: config_debug
	$(MAKE) -C ${U_CMAKE_BINARY_DIR}
build_release: config_release
	$(MAKE) -C ${U_CMAKE_BINARY_DIR}
run: build
	./build/app/${APP_EXECUTABLE_NAME}
run_release: build_release
	./build/app/${APP_EXECUTABLE_NAME}
clean_all:
	rm -rf ${U_CMAKE_BINARY_DIR}
.PHONY: clean
clean:
	$(MAKE) clean -C ${U_CMAKE_BINARY_DIR}

debug: config_debug build
	
release: config_release build_release

dependency: config_debug
	cmake -B ${U_CMAKE_BINARY_DIR} -S ./ -G ${U_CMAkE_GENERATOR} ${U_CMAKE_DEFINE} -DCMAKE_BUILD_TYPE=Debug --graphviz=${BUILD_DIR}/${DEPENDENCY_GRAPH}/${U_PROJECT_NAME}dependencyGraph.dot
	dot -Tsvg -o ${BUILD_DIR}/${DEPENDENCY_GRAPH}/${U_PROJECT_NAME}dependencyGraph.svg ${BUILD_DIR}/${DEPENDENCY_GRAPH}/${U_PROJECT_NAME}dependencyGraph.dot